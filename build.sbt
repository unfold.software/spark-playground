name := "spark-playground"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "2.4.5",
  "org.apache.spark" %% "spark-hive" % "2.4.5",
  "org.apache.spark" %% "spark-avro" % "2.4.5"
)
