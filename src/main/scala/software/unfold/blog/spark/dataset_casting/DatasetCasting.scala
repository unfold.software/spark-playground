package software.unfold.blog.spark.dataset_casting

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Dataset, Encoder, SaveMode, SparkSession}

object DatasetCasting {

  final case class StarfighterDetails(id: Long, model: String, clazz: String, manufacturer: String)

  final case class StarfighterShortDesc(id: Long, model: String)

  def main(args: Array[String]) {
    System.setSecurityManager(null)
    val spark = SparkSession.builder
      .master("local[*]")
      .appName("Simple Application")
//      .config("spark.sql.warehouse.dir", "warehouse")
      .enableHiveSupport().getOrCreate()
    import spark.implicits._

    val details: Dataset[StarfighterDetails] = Seq(
      StarfighterDetails(1, "T-65 X-wing", "Assault starfighter", "Incom Corporation"),
      StarfighterDetails(2, "Kuat RZ-1 A-wing", "Interception starfighter", "Kuat Systems Engineering"),
      StarfighterDetails(3, "Koensayr BTL Y-wing", "Bomber", "Koensayr Manufacturing")
    ).toDS()

    val shortDesc: Dataset[StarfighterShortDesc] = details.as[StarfighterShortDesc].cache()

//    details.show(false)
//    shortDesc.show(false)
//
//    val mapped = shortDesc.map(s => s)
//    mapped.show(false)
//
//    val mapped2 = shortDesc.map(identity)
//    mapped2.show(false)
//
//    val filtered = shortDesc.filter(s => s.id < 3)
//    filtered.show(false)
//    shortDesc.map(s => s.copy(id = s.id + 10)).show(false)
//
//    shortDesc.coalesce(1).write.mode(SaveMode.Overwrite).parquet("shortDesc")

    val schema: StructType = implicitly[Encoder[StarfighterShortDesc]].schema

    if (!spark.catalog.tableExists("starfighters"))
      spark.catalog.createTable("starfighters", "avro", schema, Map("path" -> "starfighters"))

    spark.sql("SHOW CREATE TABLE starfighters").show(false)
    spark.sql("describe formatted starfighters").show(false)

    shortDesc.write.mode(SaveMode.Overwrite).format("avro").save("starfighters")

    spark.table("starfighters").show(false)

    spark.stop()

  }
}
